# 一键抠图

#### 介绍
基于U2Net深度学习框架，在线一键抠图，可替换背景色，指定图片宽高以及文件大小，快速搞定证件照

#### 软件架构
前端vue3.0+element-ui
后端python+U2Net


#### 安装教程
web启动
1. 下载u2net.pth模型，存放到web/U-2-Net/saved_models/u2net目录下，[下载地址](https://drive.google.com/file/d/1ao1ovG1Qtx4b7EoskHXmi2E9rp5CHLcZ/view?usp=sharing)
1. cd web
2. pip install -r requirements.txt
3. python ./app.py
4. 访问http://localhost:8081/api/hello 连接查看是否正常启动

前端启动
1. cd html
2. npm install
3. npm run dev
